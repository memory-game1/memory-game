import React, { useState, useEffect } from "react";
import randomCards, { TOTAL_CARDS } from "./randomCards";

const MemoryGame = () => {
  const [deck, setDeck]: any = useState(randomCards());
  const [selectedCardIds, setSelectedCardIds]: any = useState([]);
  const [count, setCount] = useState(0);
  const [deckCount, setDeckCount] = useState(0);

  useEffect(() => {
    const matchedCards = deck.filter((card: any) => card.isMatched);

    if (matchedCards.length === TOTAL_CARDS) {
      alert(`Congratulations! You won in ${count} count!`);
    }
    if (matchedCards.length !== TOTAL_CARDS && deckCount === TOTAL_CARDS) {
      alert(` Try Again !`);
    }
  }, [deck, count]);

  const handleClick = (cardId: any) => {
    if (selectedCardIds.length === 2 || deck[cardId].isFlipped) {
      return;
    }

    const updatedDeck = [...deck];
    updatedDeck[cardId].isFlipped = true;

    setSelectedCardIds([...selectedCardIds, cardId]);

    if (selectedCardIds.length === 1) {
      const [cardIndex1] = selectedCardIds;
      const card1 = deck[cardIndex1];
      const card2 = deck[cardId];

      if (card1.value === card2.value) {
        updatedDeck[cardIndex1].isMatched = true;
        updatedDeck[cardId].isMatched = true;
      }

      setCount((prevcount) => prevcount + 1);
      setSelectedCardIds([]);
    }
    setDeckCount(deckCount + 1);
    setDeck(updatedDeck);
  };

  return (
    <div className="p-6">
      <div className="mb-6 w-full justify-between align-middle">
        <div className="flex justify-center  text-rose-800">Memory Game</div>
        <div>Count: {count}</div>
      </div>
      <div className="flex w-full justify-between gap-4">
        {deck.map(({ id, value, isFlipped }: any) => (
          <div
            key={id}
            onClick={() => handleClick(id)}
            className="flex h-40 w-full items-center  justify-center  bg-[red]"
          >
            <div className="text-xl text-[white]">
              {isFlipped ? value : "Hidden"}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
export default MemoryGame;
