import * as React from "react";
import { BrowserRouter } from "react-router-dom";

export const Routing = React.lazy(() => import("./routing/Routing"));

function App() {
  return (
    <React.Suspense fallback={"loadercomponent"}>
      <BrowserRouter>
        <Routing />
      </BrowserRouter>
    </React.Suspense>
  );
}

export default App;
