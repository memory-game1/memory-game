import React from "react";
import { MemoryGame, GameLayout, NotFound } from "./LazyRoute";

export const AllRoutes = () => [
  {
    path: "/",
    element: <GameLayout />,
    children: [{ path: "/", element: <MemoryGame /> }],
  },

  { path: "*", element: <NotFound /> },
];
