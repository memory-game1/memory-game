import { useRoutes } from "react-router-dom";
import { AllRoutes } from "./Route";

export default function Routing() {
  const elements = useRoutes(AllRoutes());

  return elements;
}
