// tailwind.config.js
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      // boxShadow: {
      //   shadow: "0px 0px 25px rgba(0, 0, 0, 0.1)",
      // },
    },
  },
  plugins: [
    // ...
  ],
  corePlugins: {
    preflight: false, // <== disable this!
  },
};
